from flask import Flask, request, url_for, redirect, render_template

import joblib
import pandas as pd

app = Flask(__name__)

#We need to deserialize both the model and the encoders used to create them
model = joblib.load('./model/logisticRegression_md_2_bkup_.joblib')

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/predict', methods = ['POST'])
def predict():
    features_val = [x for x in request.form.values()]

    SibSp = features_val[0]
    Parch = features_val[1]
    Fare = features_val[2]

    val = [SibSp, Parch, Fare]
    X = pd.DataFrame(val).transpose()
    X.columns = ['SibSp','Parch', 'Fare']

    X[["SibSp", "Parch"]] = X[["SibSp", "Parch"]].astype("object")
    X.Fare = X.Fare.astype("double")

    return render_template(
        'home.html', 
        pred = 'This passenger is likely to die {} with a probability of {:2.2%}'.format(
            model.predict(X), 
            (1 - model.predict_proba(X)[0][0]))
        )

if __name__ == '__main__':
    app.run(debug = True)
